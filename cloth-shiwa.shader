/*
	服しわ シェーダー by あるる（きのもと 結衣）
	Cloth Wrinkles Shader by Yui Kinomoto @arlez80

	MIT License
*/
shader_type spatial;

uniform vec4 albedo_color : hint_color = vec4( 1.0, 1.0, 1.0, 1.0 );
uniform vec3 shift = vec3( 0.0, 0.0, 0.0 );
uniform float speed = 1.5;
uniform float wave_scale = 7.454;
uniform float random_scale = 13.6;
uniform float normal_scale = 7.5;

varying vec3 local_vertex;

void vertex( )
{
	local_vertex = VERTEX;
}

vec2 random( vec2 pos )
{ 
	return fract(
		sin(
			vec2(
				dot(pos, vec2(12.9898,78.233))
			,	dot(pos, vec2(-148.998,-65.233))
			)
		) * 43758.5453
	);
}

float value_noise( vec2 pos )
{
	vec2 p = floor( pos );
	vec2 f = fract( pos );

	float v00 = random( p + vec2( 0.0, 0.0 ) ).x;
	float v10 = random( p + vec2( 1.0, 0.0 ) ).x;
	float v01 = random( p + vec2( 0.0, 1.0 ) ).x;
	float v11 = random( p + vec2( 1.0, 1.0 ) ).x;

	vec2 u = f * f * ( 3.0 - 2.0 * f );

	return mix( mix( v00, v10, u.x ), mix( v01, v11, u.x ), u.y );
}

void fragment( )
{
	vec3 shift_uv = local_vertex;
	shift_uv.x += value_noise( local_vertex.xz * random_scale + cos( local_vertex.y * random_scale ) - TIME * speed );
	shift_uv.y += value_noise( vec2( TIME * speed, cos( local_vertex.z ) ) );
	shift_uv.z += value_noise( local_vertex.yz * random_scale + cos( local_vertex.x * random_scale ) + TIME * speed );
	float x = sin( shift_uv.y * wave_scale + shift.x );
	float y = sin( shift_uv.z * wave_scale + shift.y );
	float z = sin( shift_uv.x * wave_scale + shift.z );

	ALBEDO = albedo_color.rgb;
	METALLIC = 0.0;
	ROUGHNESS = 1.0;
	NORMAL = normalize( NORMAL * normal_scale + vec3( x, y, z - 1.0 ) );
}
